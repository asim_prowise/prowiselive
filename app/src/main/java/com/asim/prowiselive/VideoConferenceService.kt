package com.asim.prowiselive

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.util.Log
import com.asim.prowiselive.constants.Constants.MSG_SHOW_JITSI_ACTI_SRVCE
import com.asim.prowiselive.service.UserCredentialsService

class VideoConferenceService : Service() {

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    private lateinit var messenger: Messenger

    /**
     * Handler of incoming messages from clients.
     */
    internal class IncomingHandler(
        context: Context,
        private val applicationContext: Context = context.applicationContext
    ) : Handler() {
        @SuppressLint("ClickableViewAccessibility")
        override fun handleMessage(msg: Message) {
            Log.d(TAG, "handleMessage received - " + msg.what)
            message = Message.obtain(msg)
            when (msg.what) {
                MSG_SHOW_JITSI_ACTI_SRVCE -> {
                    try {
                        val intentService = Intent(applicationContext, UserCredentialsService::class.java)
                        intentService.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        applicationContext.startService(intentService)
                    } catch (e: Exception) {
                        Log.d(TAG, e.toString())
                    }
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "onBind - JitsiActivityService")
        instance = this
        messenger = Messenger(IncomingHandler(this))

        return messenger.binder
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy JitsiActivityService")
        stopSelf()
    }

    companion object {
        private val TAG = "JitsiActivityService"
        lateinit var message: Message
        lateinit var instance: VideoConferenceService
    }

}