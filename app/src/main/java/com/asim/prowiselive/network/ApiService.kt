package com.asim.prowiselive.network

import com.asim.prowiselive.model.ApiResData
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("jitsi")
    fun getSSORoom(
        @Header("Authorization") tkn : String
    ): Call<ApiResData>

    @GET("jitsi/{roomName}")
    fun getJitsiLiveRoom(
        @Header("Authorization") tkn : String,
        @Path("roomName") roomName : String
    ): Call<ApiResData>

}