package com.asim.prowiselive

import android.annotation.SuppressLint
import android.app.Activity
import android.app.PictureInPictureParams
import android.content.*
import android.content.BroadcastReceiver
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Rational
import android.view.WindowManager
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.asim.prowiselive.constants.Constants
import com.asim.prowiselive.service.ControlBarService
import com.asim.prowiselive.service.ControlBarService.Companion.serviceInstance
import com.asim.prowiselive.service.LoaderWidgetService
import org.jitsi.meet.sdk.*
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.net.MalformedURLException
import java.net.URL

class RealActivity : JitsiMeetActivity() {

    private val TAG = "RealActivity"
    private lateinit var intentScreenShareWidget: Intent
    private lateinit var intentLoaderWidget: Intent
    private var ACCESS_TOKEN = ""
    private var MEETING_ROOM_NAME = ""
    private var MEETING_AUDIO_ENABLED = false
    private var MEETING_VIDEO_ENABLED = false
    private var isScreenSharing = false
    private var isAudioMuted = false
    //private var isVideoMuted = false
    private var connected = 0 // 0 = false, 1 = true, 2 = true & joined

    private var broadcastReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            this@RealActivity.onBroadcastReceived(intent)
        }
    }

    companion object {
        var instance: RealActivity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ACCESS_TOKEN = intent.getStringExtra("ACCESS_TOKEN")!!
        MEETING_ROOM_NAME = intent.getStringExtra("MEETING_ROOM_NAME")!!
        MEETING_AUDIO_ENABLED = intent.getStringExtra("MEETING_AUDIO_ENABLED")!!.toBoolean()
        MEETING_VIDEO_ENABLED = intent.getStringExtra("MEETING_VIDEO_ENABLED")!!.toBoolean()

        hideWindow(this)

        instance = this

        intentLoaderWidget = Intent(this, LoaderWidgetService::class.java)
                startService(intentLoaderWidget)
        intentScreenShareWidget = Intent(this, ControlBarService::class.java)

        allowWebViewInSystemService()

        this.registerForBroadcastMessages()

        startJitsiCall()
    }

    private fun hideWindow(activity: Activity) {
        val params: WindowManager.LayoutParams = activity.window.attributes
        params.height = 0 //fixed height
        params.width = 0 //fixed width
        params.alpha = 0f
        params.dimAmount = 0f
        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        activity.window.attributes = params
    }

    private fun showWindow(activity: Activity) {
        val params: WindowManager.LayoutParams = activity.window.attributes
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        /*params.height = resources.getDimensionPixelSize(R.dimen.px_2400)
        params.width = resources.getDimensionPixelSize(R.dimen.px_3600)*/
        params.dimAmount = 0f
        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        activity.window.attributes = params
        Handler().postDelayed({
            params.alpha = 1f
            activity.window.attributes = params
        }, 650)
    }

    private fun startJitsiCall() {
        //val urlString = "${Constants.PROWISE_MEET_URL}${MEETING_ROOM_NAME}?jwt=${ACCESS_TOKEN}"
        val urlString = "${Constants.PROWISE_MEET_URL}${MEETING_ROOM_NAME}"

        Log.d(TAG, "urlString : $urlString")
        Log.d(TAG, "ACCESS_TOKEN : $ACCESS_TOKEN")
        Log.d(TAG, "MEETING_ROOM_NAME : $MEETING_ROOM_NAME")

        val serverURL: URL = try {
            URL(urlString)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }

        val options = JitsiMeetConferenceOptions.Builder()
            .setServerURL(serverURL)
            .setToken(ACCESS_TOKEN)
            .setRoom(MEETING_ROOM_NAME)
            .setWelcomePageEnabled(false)
            /*.setServerURL(URL("https://meet.jit.si/"))
            .setRoom("asdjkgasddjk0-930-9112328")*/
            .setVideoMuted(!MEETING_VIDEO_ENABLED)
            .setAudioMuted(!MEETING_AUDIO_ENABLED)
            .setFeatureFlag("recording.enabled", false)
            .setFeatureFlag("live-streaming.enabled", false)
            .setFeatureFlag("pip.enabled", false)
            .setFeatureFlag("help.enabled", false)
            .setFeatureFlag("toolbox.alwaysVisible", false)

            .build()

        //launch(this, options)
        jitsiView!!.join(options)
    }

    fun startStopScreenSharing() {
        Handler().postDelayed({
            // shares screen explicitly
            val screenShareBroadcastIntent = BroadcastIntentHelper.buildToggleScreenShareIntent()
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(screenShareBroadcastIntent)
        }, 100)

    }

    fun disableMicrophone(mute: Boolean) {
        // mute/un-mute audio
        val audioBroadcastIntent = BroadcastIntentHelper.buildSetAudioMutedIntent(mute)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(audioBroadcastIntent)
    }

    fun disableVideo(muteVideo: Boolean) {
        // mute/un-mute video
        val videoBroadcastIntent = BroadcastIntentHelper.buildSetVideoMutedIntent(muteVideo)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(videoBroadcastIntent)
    }

    fun openCloseWindow(openWindow: Boolean) {
        // Window open close
        if (openWindow) {
            Log.d(TAG, "cameraIntent.action = START_CAMERA")
            showWindow(this)
        } else {
            Log.d(TAG, "cameraIntent.action = STOP_CAMERA")
            hideWindow(this)
        }
    }

    fun startPictureInPicture() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val pipParams = PictureInPictureParams.Builder()
                .setAspectRatio(Rational(1, 1))
                .build()
            enterPictureInPictureMode(pipParams)
        } else {
            // cannot enter PiP
        }
    }

    fun stopPictureInPicture() {
        startActivity(Intent(this, RealActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }

    fun disconnectCall() {
        connected = 0
        // Hang up explicitly
        val hangUpBroadcastIntent = BroadcastIntentHelper.buildHangUpIntent()
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(hangUpBroadcastIntent)
    }

    private fun registerForBroadcastMessages() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(BroadcastEvent.Type.CONFERENCE_JOINED.action)
        intentFilter.addAction(BroadcastEvent.Type.CONFERENCE_WILL_JOIN.action)
        intentFilter.addAction(BroadcastEvent.Type.CONFERENCE_TERMINATED.action)
        intentFilter.addAction(BroadcastEvent.Type.PARTICIPANT_JOINED.action)
        intentFilter.addAction(BroadcastEvent.Type.PARTICIPANT_LEFT.action)
        intentFilter.addAction(BroadcastEvent.Type.SCREEN_SHARE_TOGGLED.action)

        intentFilter.addAction(BroadcastEvent.Type.ENDPOINT_TEXT_MESSAGE_RECEIVED.action)
        intentFilter.addAction(BroadcastEvent.Type.CHAT_TOGGLED.action)
        intentFilter.addAction(BroadcastEvent.Type.PARTICIPANTS_INFO_RETRIEVED.action)
        intentFilter.addAction(BroadcastEvent.Type.CHAT_MESSAGE_RECEIVED.action)
        intentFilter.addAction(BroadcastEvent.Type.AUDIO_MUTED_CHANGED.action)
        intentFilter.addAction(BroadcastEvent.Type.VIDEO_MUTED_CHANGED.action)
        LocalBroadcastManager.getInstance(this).registerReceiver(this.broadcastReceiver!!, intentFilter)
    }

    // TODO this is different for every flavor : fyi check SSOAuthentication project
    /*// This is for Hermes
    @SuppressLint("DiscouragedPrivateApi")
    private fun allowWebViewInSystemService() {
        try {
            @SuppressLint("PrivateApi") val factoryClass = Class.forName("android.webkit.WebViewFactory")
            val field: Field = factoryClass.getDeclaredField("sProviderInstance")
            field.isAccessible = true
            var sProviderInstance = field.get(null)
            if (sProviderInstance != null) {
                return
            }
            val getProviderClassMethod = factoryClass.getDeclaredMethod("getProviderClass")
            getProviderClassMethod.isAccessible = true
            val factoryProviderClass = getProviderClassMethod.invoke(factoryClass) as Class<*>
            @SuppressLint("PrivateApi") val delegateClass = Class.forName("android.webkit.WebViewDelegate")
            val delegateConstructor: Constructor<*> = delegateClass.getDeclaredConstructor()
            delegateConstructor.isAccessible = true
            val chromiumMethodName: Field = factoryClass.getDeclaredField("CHROMIUM_WEBVIEW_FACTORY_METHOD")
            chromiumMethodName.isAccessible = true
            var chromiumMethodNameStr = chromiumMethodName.get(null) as String
            @Suppress("SENSELESS_COMPARISON")
            if (chromiumMethodNameStr == null) {
                chromiumMethodNameStr = "create"
            }
            val staticFactory = factoryProviderClass.getMethod(chromiumMethodNameStr, delegateClass)
            @Suppress("SENSELESS_COMPARISON")
            if (staticFactory != null) {
                sProviderInstance = staticFactory.invoke(null, delegateConstructor.newInstance())
            }
            if (sProviderInstance != null) {
                field.set("sProviderInstance", sProviderInstance)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }*/

    // This is for Eisler
    @SuppressLint("DiscouragedPrivateApi")
    fun allowWebViewInSystemService() {
        try {
            @SuppressLint("PrivateApi") val factoryClass = Class.forName("android.webkit.WebViewFactory")
            val field: Field = factoryClass.getDeclaredField("sProviderInstance")
            field.isAccessible = true
            var sProviderInstance = field.get(null)
            if (sProviderInstance != null) {
                return
            }
            val getProviderClassMethod = factoryClass.getDeclaredMethod("getProviderClass")
            getProviderClassMethod.isAccessible = true
            val factoryProviderClass = getProviderClassMethod.invoke(factoryClass) as Class<*>
            @SuppressLint("PrivateApi") val delegateClass = Class.forName("android.webkit.WebViewDelegate")
            val delegateConstructor: Constructor<*> = delegateClass.getDeclaredConstructor()
            delegateConstructor.isAccessible = true
            val chromiumMethodName: Field = factoryClass.getDeclaredField("CHROMIUM_WEBVIEW_FACTORY_METHOD")
            chromiumMethodName.isAccessible = true
            var chromiumMethodNameStr = chromiumMethodName.get(null) as String
            @Suppress("SENSELESS_COMPARISON")
            if (chromiumMethodNameStr == null) {
                chromiumMethodNameStr = "create"
            }
            val staticFactory = factoryProviderClass.getMethod(chromiumMethodNameStr, delegateClass)
            @Suppress("SENSELESS_COMPARISON")
            if (staticFactory != null) {
                sProviderInstance = staticFactory.invoke(null, delegateConstructor.newInstance())
            }
            if (sProviderInstance != null) {
                field.set("sProviderInstance", sProviderInstance)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun onBroadcastReceived(intent: Intent?) {

        if (intent != null) {
            val event = BroadcastEvent(intent)
            when (event.type) {
                BroadcastEvent.Type.CONFERENCE_JOINED -> {
                    Log.d(TAG, "onBroadcastReceived : CONFERENCE_JOINED ${event.data}")
                    connected = 2
                    stopService(intentLoaderWidget)
                    startService(intentScreenShareWidget)
                }
                BroadcastEvent.Type.CONFERENCE_WILL_JOIN -> Log.d(TAG, "onBroadcastReceived : CONFERENCE_WILL_JOIN ${event.data}")
                BroadcastEvent.Type.CONFERENCE_TERMINATED -> {
                    Log.d(TAG, "onBroadcastReceived : CONFERENCE_TERMINATED ${event.data}")
                    connected = 1
                    startActivity(Intent(this, UserFlowActivity::class.java))
                    finishAffinity()
                }
                BroadcastEvent.Type.SCREEN_SHARE_TOGGLED -> {
                    Log.d(TAG, "onBroadcastReceived : SCREEN_SHARE_TOGGLED ${event.data}")

                    isScreenSharing = event.data["sharing"].toString().toBoolean()
                    if (connected == 2) {
                        serviceInstance!!.changeScreenShareUI(isScreenSharing)
                    }
                }
                BroadcastEvent.Type.PARTICIPANT_JOINED -> Log.d(TAG, "onBroadcastReceived : PARTICIPANT_JOINED ${event.data}")
                BroadcastEvent.Type.PARTICIPANT_LEFT -> Log.d(TAG, "onBroadcastReceived : PARTICIPANT_LEFT ${event.data}")

                BroadcastEvent.Type.ENDPOINT_TEXT_MESSAGE_RECEIVED -> Log.d(TAG, "onBroadcastReceived : ENDPOINT_TEXT_MESSAGE_RECEIVED ${event.data}")
                BroadcastEvent.Type.CHAT_TOGGLED -> Log.d(TAG, "onBroadcastReceived : CHAT_TOGGLED ${event.data}")
                BroadcastEvent.Type.PARTICIPANTS_INFO_RETRIEVED -> Log.d(TAG, "onBroadcastReceived : PARTICIPANTS_INFO_RETRIEVED ${event.data}")
                BroadcastEvent.Type.CHAT_MESSAGE_RECEIVED -> Log.d(TAG, "onBroadcastReceived : CHAT_MESSAGE_RECEIVED ${event.data}")
                BroadcastEvent.Type.AUDIO_MUTED_CHANGED -> {
                    Log.d(TAG, "onBroadcastReceived : AUDIO_MUTED_CHANGED ${event.data}")
                    if (connected == 2) {
                        isAudioMuted = event.data["muted"].toString().toBoolean()
                        serviceInstance!!.changeAudioUI(isAudioMuted)
                    }
                }
                BroadcastEvent.Type.VIDEO_MUTED_CHANGED -> {
                    Log.d(TAG, "onBroadcastReceived : VIDEO_MUTED_CHANGED event.data ${event.data}")
                }
            }
        }
    }

    override fun onDestroy() {

        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver!!)
            broadcastReceiver = null
        }
        stopService(intentScreenShareWidget)
        stopService(intentLoaderWidget)

        instance = null

        super.onDestroy()

        Log.d(TAG, "onDestroy")
    }

}