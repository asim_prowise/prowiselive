package com.asim.prowiselive

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.DecelerateInterpolator
import com.asim.prowiselive.`interface`.ConferenceInterface
import com.asim.prowiselive.constants.Constants
import com.asim.prowiselive.fragments.OptionsFragment
import com.asim.prowiselive.fragments.RoomFragment
import com.asim.prowiselive.service.UserCredentialsService
import kotlinx.android.synthetic.main.activity_user_flow.*

class UserFlowActivity : AppCompatActivity(), ConferenceInterface {

    private var roomNameText = ""
    private var withAudio = false
    private var withVideo = false
    private lateinit var roomFrag: RoomFragment
    private lateinit var optionsFrag: OptionsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_flow)

        roomFrag = RoomFragment()
        optionsFrag = OptionsFragment()
        supportFragmentManager.beginTransaction().replace(R.id.mainFragLayout, roomFrag).commit()
        fadingAnimation()
    }

    private fun fadingAnimation() {
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = 1000

        val animation = AnimationSet(false)
        animation.addAnimation(fadeIn)
        mainFragLayout.startAnimation(animation)
    }

    override fun jitsiRoomName(name: String) {
        fadingAnimation()
        roomNameText = name

        val bundle = Bundle()
        bundle.putString("roomName", name)
        val transaction = this.supportFragmentManager.beginTransaction()
        optionsFrag.arguments = bundle
        transaction.replace(R.id.mainFragLayout, optionsFrag)
        transaction.commit()
    }

    override fun conferenceOptions(audio: Boolean, video: Boolean) {
        withAudio = audio
        withVideo = video
        UserCredentialsService.serviceCredInstance!!.startJitsiRequest(roomNameText, withVideo, withVideo)
        finish()
    }

    override fun goBackToCreateRoom() {
        fadingAnimation()
        supportFragmentManager.beginTransaction().replace(R.id.mainFragLayout, roomFrag).commit()
    }

    override fun performAnimation() {
        fadingAnimation()
    }

    override fun closeApp() {
        val intentSrc = Intent(this, UserCredentialsService::class.java)
        stopService(intentSrc)
        finishAffinity()
    }

    override fun endCurrentSession() {
        // sending broadcast
        val intent = Intent()
        intent.action = Constants.REQUEST_STOP_PROWISE_LIVE_SESSION
        sendBroadcast(intent)

        val intentSrc = Intent(this, UserCredentialsService::class.java)
        stopService(intentSrc)
        finishAffinity()
    }

}