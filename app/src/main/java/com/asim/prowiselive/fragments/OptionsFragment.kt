package com.asim.prowiselive.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.asim.prowiselive.R
import com.asim.prowiselive.`interface`.ConferenceInterface
import kotlinx.android.synthetic.main.fragment_options.view.*
import kotlinx.android.synthetic.main.layout_select_options.*
import kotlinx.android.synthetic.main.layout_select_options.view.*

class OptionsFragment : Fragment() {

    private var displayRoomName: String? = ""
    private var AUDIO_ENABLED = true
    private var VIDEO_ENABLED = true
    private lateinit var confInterface: ConferenceInterface

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_options, container, false)

        displayRoomName = arguments?.getString("roomName")

        confInterface = activity as ConferenceInterface

        view.textroomNaam.text = displayRoomName

        view.joinRoomBtn.setOnClickListener {
            confInterface.conferenceOptions(AUDIO_ENABLED, VIDEO_ENABLED)
        }

        view.goBackCreateRoom.setOnClickListener {
            confInterface.goBackToCreateRoom()
        }

        view.optionSettings.setOnClickListener {
            confInterface.performAnimation()
            optionSettingsView.visibility = View.VISIBLE
        }

        view.optionSettingsView.setOnClickListener {
            confInterface.performAnimation()
            optionSettingsView.visibility = View.GONE
        }

        view.backCard.setOnClickListener {
            // do nothing to stay on the card
        }

        view.iconCloseSetings.setOnClickListener {
            confInterface.performAnimation()
            optionSettingsView.visibility = View.GONE
        }

        // enable/disable video
        view.optionVideo.setOnClickListener {
            VIDEO_ENABLED = !VIDEO_ENABLED
            val videoIcon = if (VIDEO_ENABLED) {
                R.drawable.ic_baseline_videocam_24
            } else {
                R.drawable.ic_baseline_videocam_off_24
            }
            view.optionVideo.setImageResource(videoIcon)
            view.optionVideo.setColorFilter(resources.getColor(R.color.prowiseDarkBlue), android.graphics.PorterDuff.Mode.SRC_IN)
        }

        // enable/disable audio
        view.optionAudio.setOnClickListener {
            AUDIO_ENABLED = !AUDIO_ENABLED
            val audioIcon = if (AUDIO_ENABLED) {
                R.drawable.ic_baseline_mic
            } else {
                R.drawable.ic_baseline_mic_off
            }
            view.optionAudio.setImageResource(audioIcon)
            view.optionAudio.setColorFilter(resources.getColor(R.color.prowiseDarkBlue), android.graphics.PorterDuff.Mode.SRC_IN)
        }

        // options layouts
        view.soundSettingsLayout.setOnClickListener {
            showSoundSettings(true)
            showVideoSettings(false)
            showModeratorSettings(false)
        }
        view.videoSettingsLayout.setOnClickListener {
            showSoundSettings(false)
            showVideoSettings(true)
            showModeratorSettings(false)
        }
        view.moderatorSettingsLayout.setOnClickListener {
            showSoundSettings(false)
            showVideoSettings(false)
            showModeratorSettings(true)
        }

        return view
    }

    private fun showSoundSettings(show: Boolean) {
        var visible1 = View.INVISIBLE
        var visible2 = View.GONE
        if (show) {
            visible1 = View.VISIBLE
            visible2 = View.VISIBLE
        }
        colorBarSound.visibility = visible1
        selectMicrophoneLayout.visibility = visible2
        selectSpeakersLayout.visibility = visible2

    }

    private fun showVideoSettings(show: Boolean) {
        var visible1 = View.INVISIBLE
        var visible2 = View.GONE
        if (show) {
            visible1 = View.VISIBLE
            visible2 = View.VISIBLE
        }
        colorBarVideo.visibility = visible1
        selectCameraLayout.visibility = visible2
        selectVideoQualityLayout.visibility = visible2
    }

    private fun showModeratorSettings(show: Boolean) {
        var visible1 = View.INVISIBLE
        var visible2 = View.GONE
        if (show) {
            visible1 = View.VISIBLE
            visible2 = View.VISIBLE
        }
        colorBarModerator.visibility = visible1
        selectModeratorLayout.visibility = visible2
    }

}