package com.asim.prowiselive.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.asim.prowiselive.R
import com.asim.prowiselive.`interface`.ConferenceInterface
import kotlinx.android.synthetic.main.fragment_room.view.*

class RoomFragment : Fragment() {

    private lateinit var confInterface: ConferenceInterface

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_room, container, false)

        confInterface = activity as ConferenceInterface

        view.createRoomBtn.setOnClickListener {
            val roomNameText = view.roomNameEditTxt.text.toString()
            if (roomNameText.isNotEmpty()) {
                confInterface.jitsiRoomName(roomNameText)
            }
        }

        view.signOutbtn.setOnClickListener {
            confInterface.endCurrentSession()
        }

        view.closeApp.setOnClickListener {
            confInterface.closeApp()
        }

        return view

    }

}