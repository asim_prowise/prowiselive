package com.asim.prowiselive.`interface`

interface ConferenceInterface {
    fun jitsiRoomName(name:String)
    fun conferenceOptions(audio:Boolean, video:Boolean)
    fun goBackToCreateRoom()
    fun endCurrentSession()
    fun performAnimation()
    fun closeApp()
}