package com.asim.prowiselive.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.Debug
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.asim.prowiselive.R
import com.asim.prowiselive.RealActivity.Companion.instance
import com.asim.prowiselive.service.UserCredentialsService.Companion.MEETING_AUDIO_ENABLED
import com.asim.prowiselive.service.UserCredentialsService.Companion.MEETING_VIDEO_ENABLED
import kotlinx.android.synthetic.main.layout_control_bar_widget.view.*

class ControlBarService : Service() {

    private var layout_flag: Int = 0

    private lateinit var floatingView: View
    private lateinit var windowManager: WindowManager
    private var TAG = "ControlBarService"
    private var audioDisabled = false
    private var videoDisabled = false
    private var optionCheck = false
    private var openWindow = false
    private var barOpen = false
    private var pipEnabled = false
    private var isSharingMe = false
    private var screenShareButtonDisabled = false
    private val handler = Handler()
    private val delay = 1000 // 1000 milliseconds == 1 second

    companion object {
        var serviceInstance: ControlBarService? = null
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layout_flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            layout_flag = WindowManager.LayoutParams.TYPE_PHONE
        }

        floatingView = LayoutInflater.from(this).inflate(R.layout.layout_control_bar_widget, null)

        serviceInstance = this

        val layoutParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            layout_flag,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        windowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.addView(floatingView, layoutParams)
        floatingView.visibility = View.VISIBLE

        floatingView.startStopPip.alpha = 0.5f
        floatingView.startStopPip.isEnabled = false

        startMemoryBarAnimation()

        audioDisabled = !MEETING_AUDIO_ENABLED
        if (audioDisabled) {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic_off)
        } else {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic)

        }

        videoDisabled = !MEETING_VIDEO_ENABLED
        if (videoDisabled) {
            floatingView.videoBtn.setImageResource(R.drawable.ic_baseline_videocam_off_24)
        } else {
            floatingView.videoBtn.setImageResource(R.drawable.ic_baseline_videocam_24)
        }

        floatingView.startStopPip.setOnClickListener {
            pipEnabled = !pipEnabled
            if (pipEnabled) {
                instance!!.startPictureInPicture()
                if (!isSharingMe) {
                    floatingView.videoOutputScreenShare.alpha = 0.5f
                    floatingView.videoOutputScreenShare.isEnabled = false
                    screenShareButtonDisabled = true
                    // TODO other solution : make it invisible -> full screen (screenShare enabled) -> getBack to Pip -> make it visible
                }
            } else {
                instance!!.stopPictureInPicture()
                if (screenShareButtonDisabled) {
                    floatingView.videoOutputScreenShare.alpha = 1f
                    floatingView.videoOutputScreenShare.isEnabled = true
                    screenShareButtonDisabled = false
                }
            }
        }

        floatingView.layoutStopShare.setOnClickListener {
            Log.d(TAG, "layoutStopShare clicked")
            stopSelf()
            instance!!.disconnectCall()
        }

        floatingView.audioBtn.setOnClickListener {
            Log.d(TAG, "audioBtn clicked")
            audioDisabled = !audioDisabled
            if (audioDisabled) {
                instance!!.disableMicrophone(true)
            } else {
                instance!!.disableMicrophone(false)
            }
        }

        floatingView.videoBtn.setOnClickListener {
            Log.d(TAG, "videoBtn clicked videoCheck:$videoDisabled")
            videoDisabled = !videoDisabled
            if (videoDisabled) {
                instance!!.disableVideo(true)
                floatingView.videoBtn.setImageResource(R.drawable.ic_baseline_videocam_off_24)
            } else {
                instance!!.disableVideo(false)
                floatingView.videoBtn.setImageResource(R.drawable.ic_baseline_videocam_24)
            }
        }

        floatingView.optionsBtn.setOnClickListener {
            Log.d(TAG, "optionsBtn clicked")
            optionCheck = !optionCheck
            if (optionCheck) {
                floatingView.layoutVideoOptions.visibility = View.VISIBLE
            } else {
                floatingView.layoutVideoOptions.visibility = View.GONE
            }
        }

        floatingView.openCloseWindow.setOnClickListener {
            Log.d(TAG, "openCloseWindow clicked")
            openWindow = !openWindow
            instance!!.openCloseWindow(openWindow)
            if (openWindow) {
                floatingView.openCloseWindow.setImageResource(R.drawable.ic_baseline_visibility)
                floatingView.startStopPip.alpha = 1f
                floatingView.startStopPip.isEnabled = true
            } else {
                floatingView.openCloseWindow.setImageResource(R.drawable.ic_baseline_visibility_off)
                floatingView.startStopPip.alpha = 0.5f
                floatingView.startStopPip.isEnabled = false
            }
        }

        floatingView.videoOutputScreenShare.setOnClickListener {
            Log.d(TAG, "videoOutputScreenShare clicked")
            instance!!.startStopScreenSharing()
        }

        floatingView.memoryBar.setOnClickListener {
            Log.d(TAG, "memoryBar clicked")
            barOpen = !barOpen
            if (barOpen) {
                floatingView.memoryConsumptionTxt.visibility = View.VISIBLE
            } else {
                floatingView.memoryConsumptionTxt.visibility = View.GONE
            }
        }

        floatingView.setOnTouchListener(object : OnTouchListener {

            var updatepar: WindowManager.LayoutParams = layoutParams
            var x = 0.0
            var y = 0.0
            var px = 0.0
            var py = 0.0
            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                when (motionEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        x = updatepar.x.toDouble()
                        y = updatepar.y.toDouble()
                        px = motionEvent.rawX.toDouble()
                        py = motionEvent.rawY.toDouble()
                    }
                    MotionEvent.ACTION_MOVE -> {
                        updatepar.x = (x + (motionEvent.rawX - px)).toInt()
                        updatepar.y = (y + (motionEvent.rawY - py)).toInt()
                        windowManager.updateViewLayout(floatingView, updatepar)
                    }
                    else -> {
                    }
                }
                return false
            }
        })

        return START_STICKY
    }

    private fun startMemoryBarAnimation() {
        Handler().post {
            val myFadeInAnimation: Animation = AnimationUtils.loadAnimation(this, R.anim.fadeanim)
            floatingView.memoryBar.startAnimation(myFadeInAnimation)
        }

        handler.postDelayed(object : Runnable {
            override fun run() {
                postMemoryDetails()
                handler.postDelayed(this, delay.toLong())
            }
        }, delay.toLong())
    }

    fun changeScreenShareUI(isScreenSharing: Boolean) {
        isSharingMe = isScreenSharing
        if (isScreenSharing) {
            floatingView.videoOutputScreenShare.setImageResource(R.drawable.ic_baseline_screen_share)
            floatingView.recordingAnimationView.visibility = View.VISIBLE

        } else {
            if (pipEnabled) {
                screenShareButtonDisabled = true
                floatingView.videoOutputScreenShare.alpha = 0.5f
                floatingView.videoOutputScreenShare.isEnabled = false
            } else screenShareButtonDisabled = false
            floatingView.videoOutputScreenShare.setImageResource(R.drawable.ic_baseline_stop_screen_share)
            floatingView.recordingAnimationView.visibility = View.GONE
        }
    }

    fun changeAudioUI(isAudioMuted: Boolean) {
        audioDisabled = isAudioMuted
        if (isAudioMuted) {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic_off)
        } else {
            floatingView.audioBtn.setImageResource(R.drawable.ic_baseline_mic)
        }

    }

    private fun postMemoryDetails() {
        val rt = Runtime.getRuntime()
        val vmAlloc = rt.totalMemory() - rt.freeMemory()
        val nativeAlloc: Long = Debug.getNativeHeapAllocatedSize()

        val memoryConsumption = formatMemoryText(nativeAlloc + vmAlloc).toFloat()
        val memoryConsumptionString = "$memoryConsumption MB"
        floatingView.memoryConsumptionTxt.text = memoryConsumptionString

        val backgroundColor = when {
            memoryConsumption < 100.0 -> {
                // color green
                R.color.freshGreen
            }
            memoryConsumption in 101.0..149.0 -> {
                // color yellow
                R.color.freshYellow
            }
            memoryConsumption in 150.0..199.0 -> {
                // color orange
                R.color.freshOrange
            }
            memoryConsumption in 200.0..299.0 -> {
                // color orangish
                R.color.freshOrangish
            }
            else -> {
                // color red
                R.color.freshRed
            }
        }
        floatingView.memoryBar.background.setTint(resources.getColor(backgroundColor))

    }

    private fun formatMemoryText(memory: Long): String {
        val memoryInMB = memory * 1f / 1024 / 1024
        return String.format("%.1f", memoryInMB)
    }

    override fun onDestroy() {

        serviceInstance = null

        super.onDestroy()

        floatingView.visibility = View.GONE
        stopSelf()
    }
}
