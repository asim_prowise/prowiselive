package com.asim.prowiselive.service

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log
import com.asim.prowiselive.RealActivity
import com.asim.prowiselive.UserFlowActivity
import com.asim.prowiselive.broadcastReceiver.SSOBroadcastReceiver
import com.asim.prowiselive.constants.Constants
import com.asim.prowiselive.model.ApiResData
import com.asim.prowiselive.network.ApiClient
import com.asim.prowiselive.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserCredentialsService : Service() {

    private val TAG = "UserCredentialsService"
    private var MEET_USER_TOKEN = ""
    private var ACCESS_TOKEN = ""
    private var MEETING_ROOM_NAME = ""
    private var ssoBroadcastReceiver: SSOBroadcastReceiver? = null

    companion object {
        var serviceCredInstance: UserCredentialsService? = null
        var MEETING_AUDIO_ENABLED = false
        var MEETING_VIDEO_ENABLED = false
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        serviceCredInstance = this

        val filter = IntentFilter()
        filter.addAction(Constants.USER_TOKEN_PROWISE_LIVE)
        ssoBroadcastReceiver = SSOBroadcastReceiver()
        registerReceiver(ssoBroadcastReceiver, filter)

        // sending broadcast
        val intentBroadcast = Intent()
        intentBroadcast.action = Constants.REQUEST_PROWISE_LIVE_ACCESS_TOKEN
        sendBroadcast(intentBroadcast)

        return START_STICKY
    }

    fun requestRoomAccess(tkn: String) {
        MEET_USER_TOKEN = tkn
        getRoomDetails()
    }

    private fun getRoomDetails() {
        val intentFlow = Intent(applicationContext, UserFlowActivity::class.java)
        intentFlow.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intentFlow)
    }

    fun startJitsiRequest(roomName: String, audioEnabled: Boolean, videoEnabled: Boolean) {
        MEETING_AUDIO_ENABLED = audioEnabled
        MEETING_VIDEO_ENABLED = videoEnabled

        try {
            ApiClient
                .getRoomClient()
                .create(ApiService::class.java)
                .getJitsiLiveRoom("Bearer $MEET_USER_TOKEN", roomName)
                .enqueue(object : Callback<ApiResData> {
                    override fun onResponse(call: Call<ApiResData>, response: Response<ApiResData>) {
                        if (response.isSuccessful) {
                            ACCESS_TOKEN = response.body()!!.access_token
                            MEETING_ROOM_NAME = response.body()!!.room

                            startRealActivity()
                        }
                    }
                    override fun onFailure(call: Call<ApiResData>, t: Throwable) {
                        Log.e(TAG, "Error: ${t.message}")
                        stopSelf()
                    }

                })

        } catch (e: Exception) {
            Log.e(TAG, "Error: $e")
            stopSelf()
        }
    }

    private fun startRealActivity() {
        val realIntent = Intent(applicationContext, RealActivity::class.java)
        realIntent.putExtra("ACCESS_TOKEN", ACCESS_TOKEN)
        realIntent.putExtra("MEETING_ROOM_NAME", MEETING_ROOM_NAME)
        realIntent.putExtra("MEETING_AUDIO_ENABLED", MEETING_AUDIO_ENABLED.toString())
        realIntent.putExtra("MEETING_VIDEO_ENABLED", MEETING_VIDEO_ENABLED.toString())
        realIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(realIntent)
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "onDestroy UserCredentialsService")

        if (ssoBroadcastReceiver != null) {
            unregisterReceiver(ssoBroadcastReceiver)
            ssoBroadcastReceiver = null
        }
        stopSelf()
    }

}