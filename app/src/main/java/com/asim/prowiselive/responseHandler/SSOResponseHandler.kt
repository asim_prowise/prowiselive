package com.asim.prowiselive.responseHandler

import android.os.Handler
import android.os.Message
import android.util.Log
import com.asim.prowiselive.constants.Constants

class SSOResponseHandler : Handler() {

    private val TAG = "MyServiceHelper"

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            /*Constants.MSG_IS_DEVICE_REGISTERED -> {
                val responseData = msg.data["responseData"]
                Log.d(TAG, "MSG_IS_DEVICE_REGISTERED : $responseData")
            }
            Constants.MSG_IS_SERVICE_READY -> {
                val responseData = msg.data["responseData"]
                Log.d(TAG, "MSG_IS_SERVICE_READY : $responseData")
            }*/
            Constants.MSG_START_USER_FLOW -> {
                val responseData = msg.data["responseData"]
                Log.d(TAG, "MSG_START_USER_FLOW: $responseData")
            }
        }
        super.handleMessage(msg)
    }
}