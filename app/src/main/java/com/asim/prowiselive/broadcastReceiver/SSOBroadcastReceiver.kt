package com.asim.prowiselive.broadcastReceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.asim.prowiselive.CredentialsActivity.Companion.instance
import com.asim.prowiselive.service.UserCredentialsService
import com.asim.prowiselive.constants.Constants.SCREEN_SHARING_TOKEN_BROADCAST_FAILURE
import com.asim.prowiselive.constants.Constants.SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS
import com.asim.prowiselive.constants.Constants.USER_TOKEN_PROWISE_LIVE

class SSOBroadcastReceiver : BroadcastReceiver() {

    private val TAG = "SSOBroadcastReceiver"

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            SCREEN_SHARING_TOKEN_BROADCAST_SUCCESS -> {
                val deviceToken = intent.getStringExtra("access_token").toString()
                instance.requestRoomAccess(deviceToken)
                Log.d(TAG, "token success = $deviceToken")
            }
            SCREEN_SHARING_TOKEN_BROADCAST_FAILURE -> {
                val failure = intent.getStringExtra("access_token").toString()
                instance.tokenFailure(failure)
                Log.e(TAG, "token failure = $failure")
            }
            USER_TOKEN_PROWISE_LIVE -> {
                val deviceToken = intent.getStringExtra("access_token").toString()
                UserCredentialsService.serviceCredInstance!!.requestRoomAccess(deviceToken)
                Log.d(TAG, "token success = $deviceToken")
            }
        }
    }
}